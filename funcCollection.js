$(document).ready(function () {
    $("#lotteryGame").click(function () {
        //if()
        let userInput = $("#number").val();
        if (userInput === "") {
            alert("กรุณาใส่ตัวเลขด้วยค่ะ");
            // let audio = new Audio("Audio/fail.mp3");
            // audio.play();
            return;
        }
        else if (isNotANumber(userInput)) {
            alert("โปรแกรมนี้รองรับแค่ตัวเลขเท่านั้นค่ะ");
            return;
        }
        else if (!isLengthTwo(userInput)) {
            alert("โปรแกรมนี้รองรับแค่ตัวเลข 2 ตัวเท่านั้นค่ะ");
            return;
        }
        // statistics();
        lotteryWinningChance(userInput);
        //alert("ตัวเลขของคุณ: " + userInput);

    });

    //-------------------------------------------------------------------------------
    $("#buttonRandom").dblclick(function () {
        //let item = null;
        //let item = Math.floor(Math.random() * 100);
        $("#buttonRandom").hide();
        let item = getRandomArbitrary(10, 99);
        $('#ans').html(item).hide();
        $('#ans').html(item).fadeIn(3000);
        $('#ans').html(item).fadeOut(6000);
        //sleep(500);
        //$("#buttonRandom").show();
        $('#buttonRandom').fadeIn(12000);
    });
    //-----------------------------------------------------------------------------
    //    $("button").click(function(){
    //         $("#ans").fadeOut();
    //         $("#ans").fadeOut("slow");
    //         $("#ans").fadeOut(3000);
    //     });
    //-----------------------------------------------------------------------------
});

// function statistics() {
//     $.get('past_record.json', function (data) {
//         //-----------eg. {15: 1, 97: 7}  (number: times)
//         let arrLWC = {};
//         for (let element in data) {
//             if (arrLWC.hasOwnProperty(data[element])) {
//                 arrLWC[data[element]] += 1;
//             }
//             else {
//                 arrLWC[data[element]] = 1;
//             }
//         }
//         let perAllNumbers = {};//เก็บpercent การถูกของแต่ละตัวเลข
//         for (let i = 10; i <= 99; i++) {
//             if (!perAllNumbers.hasOwnProperty(i)) //userInput เลขท้ายสองตัว
//             {

//                 if (!arrLWC.hasOwnProperty(i)) {
//                     perAllNumbers[i] = 0;
//                 }
//                 else {
//                     perAllNumbers[i] = ((arrLWC[i] / data.length) * 100).toFixed(2);
//                 }

//                 //alert("ตัวเลขของคุณ: " + userInput + " มีโอกาสถูก 0%");
//             }
//         }
//         console.log(perAllNumbers);
//         //find mean
//         let tempM = 0;
//         for (let i = 10; i <= 99; i++) {
//             tempM += parseInt(perAllNumbers[i]);
//         }
//         let Mean = (tempM / 90).toFixed(2);
//         //console.log(Mean);
//         
//         return Mean;
//     });
// }


function lotteryWinningChance(userInput) {
    //get data from past_record.json and calculate the % of winning
    $.get('past_record.json', function (data) {
        //-----------eg. {15: 1, 97: 7}  (number: times)
        let arrLWC = {};
        for (let element in data) {
            if (arrLWC.hasOwnProperty(data[element])) {
                arrLWC[data[element]] += 1;
            }
            else {
                arrLWC[data[element]] = 1;
            }
        }
        //console.log("length " +Object.keys(arrLWC).length);

        //Find Mean
        let perAllNumbers = {};//เก็บpercent การถูกของแต่ละตัวเลข
        for (let i = 10; i <= 99; i++) {
            if (!perAllNumbers.hasOwnProperty(i)) //userInput เลขท้ายสองตัว
            {

                if (!arrLWC.hasOwnProperty(i)) {
                    perAllNumbers[i] = 0;
                }
                else {
                    perAllNumbers[i] = ((arrLWC[i] / data.length) * 100).toFixed(2);
                }

                //alert("ตัวเลขของคุณ: " + userInput + " มีโอกาสถูก 0%");
            }
        }
        console.log(perAllNumbers);
        //find mean
        let tempM = 0;
        for (let i = 10; i <= 99; i++) {
            tempM += parseInt(perAllNumbers[i]);
        }
        let Mean = (tempM / 90).toFixed(2);
        console.log("Mean: " + Mean);
        //------------------------------------
        //------------------------------------
        //มากกว่า mean = success, น้อยกว่า mean = fail
        if (perAllNumbers[userInput] > Mean) {
            //let audio = new Audio("Audio/success.mp3");
            //audio.play();
            document.getElementById('success').play();
            alert("ยินดีด้วยจร้า"); //.toFixed(n) = round to n decimal places
        }
        else {
            //let audio = new Audio("Audio/fail.mp3");
            //audio.play();
            document.getElementById('fail').play();
            alert("กินแห้วไปน้า");
        }

        // if (!arrLWC.hasOwnProperty(userInput)) //userInput เลขท้ายสองตัว
        // {
        //     alert("ตัวเลขของคุณ: " + userInput + " มีโอกาสถูก 0%");
        // }
        // else {
        //     let per = 0;
        //     per = ((arrLWC[userInput] / data.length) * 100).toFixed(2);
        //     alert("ตัวเลขของคุณ: " + userInput + " มีโอกาสถูก " + per + "%"); //.toFixed(n) = round to n decimal places
        // }

    });

}

function isNotANumber(num) {
    for (let i = 0; i < num.length; i++) {
        if (num[i] !== '0' && num[i] !== '1' && num[i] !== '2' && num[i] !== '3' && num[i] !== '4' && num[i] !== '5' && num[i] !== '6' && num[i] !== '7' && num[i] !== '8' && num[i] !== '9') {
            return true;
        }
    }
    return false;
}

function isLengthTwo(num) {
    if (num.length == 2) {
        return true;
    }
    return false;
}

function getRandomArbitrary(min, max) {
    if ((Math.random() * (max - min)) % 1 != 0) //if decimal nnumber
    {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    else {
        return Math.random() * (max - min) + min;
    }

}